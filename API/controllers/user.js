const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// controller for creating new user
module.exports.registerUser = (reqBody) =>{

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),

	})

	// condition for checking if the email already exist.
	return User.find({email : reqBody.email}).then(result => {

		if(result.length > 0 ){
			console.log("email already exist");
			return("email already exist, failed to register")

		}else{
			return newUser.save().then((user, error)=>{
				if(error){
					console.log("failed to register")
					return false
				}else{
					console.log("successfully register")
					return true
					
				}
			})
		}
	})
}


//USER details controller
module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {

				
				result.password = "";

				
				return result;

			});

		};

// login controller
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false
		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			}
		}
	})
}


// Set user as admin
module.exports.setAsAdmin = (reqParams) => {

	let updateIsAdmin = {
		isAdmin : true
	}

	return User.findById(reqParams.userId).then(result =>{
		if(result.isAdmin){
			return("No need to change account type. This user is already an admin")
		}else{
			return User.findByIdAndUpdate(reqParams.userId, updateIsAdmin).then((course, err)=> {
				if(err){
					return false
				}else{
					console.log("successfully update user to admin")
					return true
				}
			})
		}
	})
	
}

// - Non-admin User checkout (Create Order)
module.exports.checkout = async (data) => {

	let isOrderUpdated = await User.findById(data.userId).then(user =>{

		return Product.findById(data.productId).then(product =>{
			
			
			if(data.quantity < 1 || data.quantity == null){
				data.quantity = 1;
			}

			let orderData = new Order({

				totalAmount : product.price * data.quantity,
				user: {
					userId: data.userId,
					userEmail: user.email
				},
				product: {
					productId: data.productId,
					productName: product.name,
					quantity: data.quantity
				}
			})

			return orderData.save().then((addOrder, error) => {
				if(error) {
					console.log(error)
				} else {
					console.log("Order save")

				}

				let userOrderData = {

					orderId: orderData.id,
					totalAmount: orderData.totalAmount,
					product: {
						productId: data.productId,
						productName: product.name
					}
				}

				user.order.push(userOrderData)

				return user.save().then((userUpdate, error) => {
					if(userUpdate&&addOrder) {
						console.log("user order details save.")
						return true
					} else {
						return false

					}
				})
			})			
		})

	})
	if(isOrderUpdated) {
		return true
	} 
	else {
		return false
	}
}

// - Retrieve authenticated user’s orders
module.exports.getOrder = (data) => {

	return Order.find({"user.userId": data.userId}).then(result =>{
		return result
		console.log(result)
	})
}

// - Retrieve all orders (Admin only)
module.exports.getAllOrder = () => {
	return Order.find({}).then(result =>{
		return result
	})
}